import { Component, OnInit } from '@angular/core';
import { CourseService } from '../course.service';
import { Course } from './../course';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.css']
})
export class ScheduleComponent implements OnInit {
  private courseList:Course[];
  constructor(private courseService:CourseService) {
    
   }

  ngOnInit() {
  }

}
