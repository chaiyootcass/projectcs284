import { Component, OnInit } from '@angular/core';
import { CourseService } from '../course.service';
import { Router } from "@angular/router";
import { Course } from '../course';
 
@Component({
  selector: 'app-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.css']
})
export class CourseComponent implements OnInit {
  private courseList:Course[];
  private searchcourse:string="";
  constructor(private courseService:CourseService,private router: Router) { }

  private searchCourse():void{
    /*+++++++++++++++++++++++++++++++++++++++++++++++*/
    this.courseService.searchCourses(this.searchcourse).subscribe(courses=> {
      if (courses != undefined){
       this.courseList = courses;
       console.log(this.courseList[0].codeName);
      }else{
       this.courseList = [];
      }
      });
  }

  ngOnInit() {
  }

}
