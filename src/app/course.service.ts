import { Injectable } from '@angular/core';
import { Http, Headers, Response } from "@angular/http";
import { Observable,of } from "rxjs";
import { catchError } from 'rxjs/operators';
import { map } from 'rxjs/operators';
import { Router } from "@angular/router";
import { Course } from './course';
import { Codename } from './codename';
@Injectable({
  providedIn: 'root'
})
export class CourseService {
  private baseurl:string = "https://projectcs284.000webhostapp.com/";

  constructor(private http: Http,private router: Router) { }
  public searchCourses(key:String):Observable<Course[]>{
    // return this.courses;
    let url = this.baseurl + "searchSubject.php";
    //let body = "&key="+key+"&key1="+key+"&key2="+key+"&key3="+key+"&key4="+key;
    let body = "&key="+key;
    let header = { headers: new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' }) };
    let callUrl: string = url + "?" + body;
    console.log("calling: " + callUrl);
    return this.http.post(url, body, header)
    .pipe(map((res: Response) => {
      let data = res.json();
    if (data.message != "Success") {
      console.log("error: " + data.Message);
      return [];
    } else {
      let arr:Course[] =[];
      for (let dbCourse of data.data){
        console.log(dbCourse.startTime);
        let c:Course = new Course(dbCourse.id,dbCourse.codeName,dbCourse.subjectName,dbCourse.credit
          ,dbCourse.year,dbCourse.term,dbCourse.day,dbCourse.startTime,dbCourse.endTime,dbCourse.room
          ,dbCourse.speaker,dbCourse.program,dbCourse.section,dbCourse.yearOfStudy);
        //console.log(note.getText())
        arr.push(c);
      } 
      return arr;
    }
    })
    
    )
    .pipe(catchError((error: any) => {
      if (error.status == 404){
       // console.log ("code note exist" );
        return of([]);
      }else{
       // console.log("throw error" + JSON.stringify(error));
        return Observable.throw(error);
      }
    }))
  }
  // public getCourses():Observable<Course[]>{
  //   // return this.courses;
  //   let url = this.baseurl + "listsubjectall.php";
  //   let body = "";
  //   let header = { headers: new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' }) };
  //   let callUrl: string = url ;
  //   console.log("calling: " + callUrl);
  //   return this.http.post(url, body, header)
  //   .pipe(map((res: Response) => {
  //     let data = res.json();
  //   if (data.message != "Success") {
  //    // console.log("error: " + data.Message);
  //     return [];
  //   } else {
  //     let arr:Course[] =[];
  //     for (let dbCourse of data.data){
  //       console.log(dbCourse.startTime);
  //       let c:Course = new Course(dbCourse.id,dbCourse.codeName,dbCourse.subjectName,dbCourse.credit
  //         ,dbCourse.year,dbCourse.term,dbCourse.day,dbCourse.startTime,dbCourse.endTime,dbCourse.room
  //         ,dbCourse.speaker,dbCourse.program,dbCourse.section,dbCourse.yearOfStudy);
  //       //console.log(note.getText())
  //       arr.push(c);
  //     } 
  //     return arr;
  //   }
  //   })
    
  //   )
  //   .pipe(catchError((error: any) => {
  //     if (error.status == 404){
  //      // console.log ("code note exist" );
  //       return of([]);
  //     }else{
  //      // console.log("throw error" + JSON.stringify(error));
  //       return Observable.throw(error);
  //     }
  //   }))
  // }
  public getCodeID():Observable<Codename[]>{
    // return this.courses;
    let url = this.baseurl + "listsubjectall.php";
    let body = "";
    let header = { headers: new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' }) };
    let callUrl: string = url ;
    //console.log("calling: " + callUrl);
    return this.http.post(url, body, header)
    .pipe(map((res: Response) => {
      let data = res.json();
    if (data.message != "Success") {
     // console.log("error: " + data.Message);
      return [];
    } else {
      let arr:Codename[] =[];
      for (let dbCourse of data.data){
        //console.log(dbCourse.startTime);
        let c:Codename = new Codename(dbCourse.codeID,dbCourse.codeName,dbCourse.credit,dbCourse.program,dbCourse.subjectName
          ,dbCourse.year);
        //console.log(note.getText())
        arr.push(c);
      } 
      return arr;
    }
    })
    
    )
    .pipe(catchError((error: any) => {
      if (error.status == 404){
       // console.log ("code note exist" );
        return of([]);
      }else{
       // console.log("throw error" + JSON.stringify(error));
        return Observable.throw(error);
      }
    }))
  }
  public addCourse(codeID:String,
    term:String,day:String,startTime:String,endTime:String,room:String,speaker:String,section:String,yearOfStudy:String){
    let url = this.baseurl+"addSubject.php";
    let body = "&codeID="+codeID+"&term="+term+"&day="+day+"&startTime="+startTime+"&endTime="+endTime+"&room="+room
    +"&speaker="+speaker+"&section="+section+"&yearOfStudy="+yearOfStudy;
    let header = { headers: new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' }) };
    let callUrl: string = url + "?" + body;
    console.log("add couse calling: " + callUrl);
    return this.http.post(url, body, header)
    .pipe(map((res: Response) => {
      let data = res.json();
      if (data.message == "Success"){
        alert("เพิ่มสำเร็จ")
    //  console.log("add success");
      return true;
      }else{
        alert("เพิ่มไม่สำเร็จ")
     // console.log("add fail");
      return false;
      }
    }))
    .pipe(catchError((error: any) => {
     // console.log("throw error" + JSON.stringify(error));
      return Observable.throw(error); 
    })) 
  }
}
