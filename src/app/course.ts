export class Course {
    // public id:number;
    // public code:string;
    // public name:string;

    public constructor(public id:number, public codeName:string, 
        public subjectName:string,public credit:number,public year:number, 
        public term:number,public day:string,public startTime:string,
        public endTime:string,public room:string,public speaker:string,
        public program:string,public section:string,public yearOfStudy:string){
        // this.id = id;
        // this.code = code;
        // this.name = name;
    }
    public getcodeName():string{
        return this.codeName;
    }
       
    

 
}