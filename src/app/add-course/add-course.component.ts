import { Component, OnInit } from '@angular/core';
import { CourseService } from '../course.service';
import { Codename } from '../codename';

@Component({
  selector: 'app-add-course',
  templateUrl: './add-course.component.html',
  styleUrls: ['./add-course.component.css']
})
export class AddCourseComponent implements OnInit {
  private term:String;
  private day:String;
  // private startTime:String;
  // private endTime:String;
  private room:String;
  private speaker:String;
  
  private section:String;
  private yearOfStudy:String;
  private codeID:String;

  private hrStart:String;
  private minStart:String;
  private hrEnd:String;
  private minEnd:String;

  private listCodeName:Codename[]=[];

  constructor(private courseService:CourseService) { 
    this.courseService.getCodeID().subscribe(courses=> {
      if (courses != undefined){
       this.listCodeName = courses;
     
      }else{
       this.listCodeName = [];
      }
      });
  
  }

  private addCourse():void{
    /*+++++++++++++++++++++++++++++++++++++++++++++++*/
   // this.login.Checkuser(this.username,this.password).subscribe();
    this.courseService.addCourse(this.codeID,
      this.term,this.day,this.hrStart+":"+this.minStart+":00",this.hrEnd+":"+this.minEnd+":00",this.room,this.speaker,this.section,this.yearOfStudy).subscribe();

  }

  ngOnInit() {
  }
  private test():void{
    console.log(this.codeID);
  }

}
