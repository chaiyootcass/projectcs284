import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { LoginService } from './login.service'
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HttpClientModule } from '@angular/common/http';
import { HomeComponent } from './home/home.component';
import { CourseService } from './course.service';
import { AddCourseComponent } from './add-course/add-course.component';
import { ScheduleComponent } from './schedule/schedule.component';
import { CourseComponent } from './course/course.component';
import { Schedule1Component } from './schedule1/schedule1.component';
import { Schedule2Component } from './schedule2/schedule2.component';
import { Schedule3Component } from './schedule3/schedule3.component';

const routes: Routes = [   
  { path: 'home', component: HomeComponent },  
  { path: 'login', component: LoginComponent },
  { path: 'addcourse', component: AddCourseComponent },
  { path: 'schedule', component: ScheduleComponent },
  { path: 'course', component: CourseComponent },
  { path: 'schedule1', component: Schedule1Component },
  { path: 'schedule2', component: Schedule2Component }, 
  { path: 'schedule3', component: Schedule3Component }, 
  { path: '', redirectTo: '/login', pathMatch: 'full' }
 ]; 
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    AddCourseComponent,
    ScheduleComponent,
    CourseComponent,
    Schedule1Component,
    Schedule2Component,
    Schedule3Component,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    FormsModule,
    HttpModule
  ],
  providers: [CourseService,LoginService],
  bootstrap: [AppComponent]
})
export class AppModule { }
