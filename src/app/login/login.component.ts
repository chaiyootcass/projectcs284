import { Component, OnInit } from '@angular/core';
import { LoginService } from '../login.service'
import { Router } from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  private username:string="";
  private password:string="";
  constructor(private loginService:LoginService,private router: Router) {
    
   }
  private checkUser():void{
    this.loginService.Checkuser(this.username,this.password).subscribe();
  }
  ngOnInit() {

  }

  
}
