import { Component, OnInit } from '@angular/core';
import { CourseService } from '../course.service';
import { LoginService } from '../login.service'
import { Router } from "@angular/router";
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private loginService:LoginService,private courseService:CourseService,private router: Router) { 
    // this.loginService.checkLogin();
  }
  private logout():void{
    this.loginService.login=false;
    this.router.navigate(["/login"]);
  }
  ngOnInit() {
  }

}
